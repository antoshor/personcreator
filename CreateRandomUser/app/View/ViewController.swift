//
//  ViewController.swift
//  TestProject
//
//  Created by Mac Admin on 28.04.2022.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: IBOutlets
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var blueView: UIView!
    @IBOutlet weak var nickname: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: IBAction
    
    @IBAction func createAnotherUser(_ sender: Any) {
        create()
        createButton.isHidden = true
    }
    
    //MARK: property
    
    var presenter: PresenterProtocol?
    var timer: Timer?
    var progress: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = Presenter(view: self)
        
        setupView()
        setupTable()
        
    }
    
    //MARK: setup function
    
    private func setupView() {
        img.layer.cornerRadius = img.frame.size.width / 2
        img.clipsToBounds = true
        
        blueView.layer.cornerRadius = blueView.frame.size.width / 2.3
        blueView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        blueView.clipsToBounds = true
        
        createButton.layer.cornerRadius = 20
        createButton.clipsToBounds = true
    }
    
    private func setupTable() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    //MARK: strart logic func
    
    private func create() {
        presenter?.getData()
    }
}

//MARK: extension ViewProtocol

extension ViewController: ViewProtocol {
    
    func getInfo(username: String, email: String) {
        self.nickname.text = username
        self.email.text = email
    }
    
    func showPhoto(photo: UIImage) {
        img.image = photo
    }
    
    func reloadTable() {
        tableView.reloadData()
    }
    
    func showCreateButton() {
        createButton.isHidden = false
    }
    
    func startLoadData() {
        activityIndicator.startAnimating()
    }
    
    func stopLoadData() {
        activityIndicator.stopAnimating()
    }
}

//MARK: extension UITableViewDelegate, UITableViewDataSource

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.tableModel?.tableArray.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as! CustomTableViewCell
        cell.textLable.text = presenter?.tableModel?.tableArray[indexPath.row]
        return cell
    }
}
