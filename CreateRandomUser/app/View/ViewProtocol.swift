//
//  ViewProtocol.swift
//  TestProject
//
//  Created by Mac Admin on 01.05.2022.
//

import Foundation
import UIKit

protocol ViewProtocol: AnyObject {

    func getInfo(username: String, email: String)
    
    func reloadTable()
    
    func showCreateButton()
    
    func showPhoto(photo: UIImage)
    
    func startLoadData()
    
    func stopLoadData()
    
     
}
