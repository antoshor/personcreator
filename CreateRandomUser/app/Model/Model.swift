//
//  Model.swift
//  TestProject
//
//  Created by Mac Admin on 01.05.2022.
//

import Foundation

// MARK: - TableModel
struct TableModel {
    
    var name: String = "Имя не указано"
    var age: String = "Возраст не указан"
    var country: String = "Страна не указана"
    var phone: String = "Телефона нет"
    
    var tableArray = [String]()
    
    init(name: String, age: String, country: String, phone: String) {
        self.name = name
        self.age = age
        self.country = country
        self.phone = phone
        
        tableArray.append("Имя: \(name)")
        tableArray.append("Возраст: \(age)")
        tableArray.append("Страна: \(country)")
        tableArray.append("Телефон: \(phone)")
    }
    
}

// MARK: - PersonModel
struct PersonModel: Codable {
    let results: [Result]?
}

// MARK: - Result
struct Result: Codable {
    let gender: String?
    let name: Name?
    let email: String?
    let dob: Dob?
    let location: Location?
    let phone, cell: String?
    let login: Login?
    let picture: Picture?
}

// MARK: - Location
struct Location: Codable {
    let city, state, country: String?
}

// MARK: - Login
struct Login: Codable {
    let uuid, username, password: String?
}

// MARK: - Dob
struct Dob: Codable {
    let date: String?
    let age: Int?
}

// MARK: - Name
struct Name: Codable {
    let title, first, last: String?
}

// MARK: - Picture
struct Picture: Codable {
    let large: String?
}
