//
//  NetworkService.swift
//  TestProject
//
//  Created by Mac Admin on 02.08.2022.
//

import Foundation

class NetworkService {
    
    static let shared = NetworkService()
    
    func getPost(url: String, completion: @escaping (PersonModel?)->() ) {
        
        guard let url = URL(string: url) else { return }
        
        let request = URLRequest(url: url)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            if let data = data {
                
                if let decode = try? JSONDecoder().decode(PersonModel.self, from: data) {
                    
                    completion(decode)
                    
                }
                else {
                    print("Ошибка в парсинге данных")
                    
                    completion(nil)
                }
            } else {
              print("Ошибка в получении data")
                
              completion(nil)
            }
        }
        task.resume()
    }
    
}
