//
//  Presenter.swift
//  TestProject
//
//  Created by Mac Admin on 01.05.2022.
//

import Foundation
import UIKit

class Presenter: PresenterProtocol {
    
    //MARK: property
    
    weak var view: ViewProtocol?
    var tableModel: TableModel?
    
    let url = "https://randomuser.me/api/"
    let group = DispatchGroup()
    var timer: Timer?
    var counter = 0
    
    //MARK: main logic func
    
    func getData() {
        view?.startLoadData()
        timerFunc()
        
        NetworkService.shared.getPost(url: url) { [weak self] info in
            if let self = self {
                let queue = DispatchQueue(label: "Queue", attributes: .concurrent)
                
                self.group.enter()
                queue.async {
                    if let url = info?.results?.first?.picture?.large {
                        
                        if let data = try? Data(contentsOf: URL(string: url)!) {
                            
                            let img = UIImage(data: data)
                            
                            DispatchQueue.main.async {
                                self.view?.showPhoto(photo: img!)
                                self.group.leave()
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.view?.showPhoto(photo: UIImage(named: String("default"))! )
                            self.group.leave()
                        }
                    }
                }
                
                if let data = info?.results?.first {
                    
                    let titleName = data.name?.title ?? ""
                    let firstName = data.name?.first ?? ""
                    let lastName = data.name?.last ?? ""
                    
                    let name = "\(titleName) \(firstName) \(lastName)"
                    
                    let ageT = data.dob?.age ?? 0
                    let age = String(ageT)
                    
                    let country = data.location?.country ?? "Страна не указана"
                    
                    let phone = data.phone ?? "Телефон не указан"
                    
                    self.tableModel = TableModel(name: name, age: age, country: country, phone: phone)
                    
                    DispatchQueue.main.async {
                        let username = data.login?.username ?? "Имя не задано"
                        let email = data.email ?? "Почта не указана"
                        
                        self.view?.getInfo(username: username, email: email)
                    }

                } else {
                    print("В ответе от NetworkService пришел info = nil")
                }
                
                self.group.notify(queue: .main) {
                    self.view?.reloadTable()
                    self.view?.showCreateButton()
                    self.view?.stopLoadData()
                }
            }
        }
    }
    
    //MARK: timer function
    // the timer is used when a response from the NetworkService does not come for a long time
    
    private func timerFunc() {
        let timer = Timer(timeInterval: 1, target: self , selector: #selector(tic), userInfo: nil, repeats: true)
        timer.fire()
    }
    
    @objc private func tic() {
        if counter == 10 {
            timer?.invalidate()
            counter = 0
            view?.stopLoadData()
        }
        counter+=1
        
    }
    
    required init(view: ViewProtocol) {
        self.view = view
        getData()
    }
    
}
