//
//  PresenterProtocol.swift
//  TestProject
//
//  Created by Mac Admin on 01.05.2022.
//

import Foundation

protocol PresenterProtocol {
   
    var tableModel: TableModel? {get set}
    
    func getData()
    
    init(view: ViewProtocol)
}
