# PersonCreator


В этом небольшом приложении я учился работать с URLSession. Все что оно делает - получает и парсит данные с бесплатного api (https://randomuser.me/api/) и выводит их на экран.

## Результат

<img src="F1.png" alt="drawing" width="200"/>
<img src="F2.png" alt="drawing" width="200"/>
<img src="F3.png" alt="drawing" width="200"/>
